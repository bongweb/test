<?php

namespace App;

class CreateFile implements \Iterator {


    private $numbers=[];
    private $position=0;
    public function __construct()
    {
        $this->position = 0;
        for($i=0; $i<=10000000;$i++ ){
            $this->numbers[strval($i)]= rand(0,99);
        }

        asort($this->numbers);
    }


    public function rewind() {

        $this->position = 0;
    }

    public function current() {

        return $this->numbers[$this->position];
    }

    public function key() {

        return $this->position;
    }

    public function next() {

        ++$this->position;
    }

    public function valid() {

        return isset($this->numbers[$this->position]);
    }
}

$nums = new CreateFile();
file_put_contents('numbers.txt','');
foreach ($nums as $key => $num){
    file_put_contents('numbers.txt',$num.PHP_EOL,FILE_APPEND);
}