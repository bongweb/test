<?php
namespace App;

class  ReadFile{

    public function __construct()
    {
    }

    public static function read(){
       var_dump('current memory '. memory_get_usage(). PHP_EOL);        // 333200

        $file = new \SplFileObject(__DIR__.'/numbers.txt');  // 996kb
        $file->seek(5000);                       // jump to line 5000 (zero-based)
        var_dump('current file '.$file->current(). PHP_EOL);          // output current line

        var_dump('after storage memory '. memory_get_usage(). PHP_EOL);
    }

    public static function write(){
        $values = file_get_contents(__DIR__.'/numbers.txt');
        $values = str_replace(array("\r\n", "\r", "\n"), " ", $values);
        $values = explode(' ',$values);
        asort($values);
        $fp = fopen(__DIR__.'/new-file.txt', 'w');
        foreach ($values as $key => $value){
            fwrite($fp, $value);
        }

        fclose($fp);
    }

}
ReadFile::read();
ReadFile::write();